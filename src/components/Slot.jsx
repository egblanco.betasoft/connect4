import React from "react";
import style from "../components/app.module.css";

const Slot = ({ slot, x, y, handleClickSlot }) => {
  const handleSlotColor = (value) => {
    if (value === 1) return style.yellow_slot;
    if (value === 2) return style.red_slot;
    return style.white_slot;
  };

  return (
    <div
      onClick={() => handleClickSlot(x, y)}
      className={`${style.slot} ${handleSlotColor(slot)}`}
    ></div>
  );
};

export default Slot;
