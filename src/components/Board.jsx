import React, { useEffect, useState } from "react";
import Slot from "./Slot";
import style from "../components/app.module.css";

const PLAYERS = ["YELLOW", "RED"];
const WIN_PLAYERS = ["RED", "YELLOW"];

const Board = ({ slots, setSlots }) => {
  const [currentPlayer, setCurrentPlayer] = useState(1);
  const [winner, setWinner] = useState(0);

  useEffect(() => {
    handleHorizontalWinner(currentPlayer);
    handleVerticalWinner(currentPlayer);
    handleDiagonalWinner(currentPlayer);
  }, [slots]);

  const handleClickSlot = (indexY, indexX) => {
    if (winner !== 0) return;
    setCurrentPlayer((prev) => (prev === 1 ? 2 : 1));
    const tempSlots = [...slots];
    if (slots[0][indexX] !== 0) return;
    for (let i = slots.length - 1; i >= 0; i--) {
      if (slots[i][indexX] === 0) {
        tempSlots[i][indexX] = currentPlayer;
        setSlots(tempSlots);
        return;
      }
    }
    setSlots(tempSlots);
  };

  const handleDiagonalWinner = (player) => {
    let counter = 0;
    let value = 0;
    for (let y = 3; y >= 0; y--) {
      let initialY = y;
      for (let x = 0; x <= slots[0].length; x++) {
        if (initialY >= slots.length - 1) break;
        value = slots[initialY][x];
        let prevValue =
          initialY === 3 && value !== 0 ? slots[initialY][x] : value;

        initialY = initialY + 1;

        if (prevValue === value && value !== 0) {
          counter++;
          if (counter === 3) {
            setWinner(player);
            return;
          }
        } else counter = 0;
      }
    }

    for (let y = 0; y < 3; y++) {
      let initialY = 0;
      for (let x = y + 1; x <= slots[0].length - 1; x++) {
        if (initialY >= slots.length - 1) break;
        initialY = initialY + 1;
        let prevValue =
          initialY === 1 && value !== 0 ? slots[initialY][x] : value;
        value = slots[initialY][x];

        if (prevValue === value && value !== 0) {
          counter++;
          if (counter === 3) {
            setWinner(player);
            return;
          }
        } else counter = 0;
      }
    }

    for (let x = 3; x <= slots[0].length - 1; x++) {
      let initialX = x;
      for (let y = 0; y <= slots.length; y++) {
        initialX = initialX - 1;
        if (initialX < 0) break;
        value = slots[y][initialX];
        let prevValue =
          initialX === 1 && value !== 0 ? slots[y][initialX] : value;
        if (prevValue === value && value !== 0) {
          counter++;
          if (counter === 4) {
            setWinner(player);
            return;
          }
        } else counter = 0;
      }
    }

    let initialX = 0;
    for (let y = 6; 3 < y; y--) {
      let initialY = 6;
      initialX = initialX + 1;
      for (let x = initialX; x <= slots[0].length - 1; x++) {
        value = slots[initialY][x];
        let prevValue =
          initialX === 1 && value !== 0 ? slots[initialY][x] : value;
        if (prevValue === value && value !== 0) {
          counter++;
          if (counter === 4) {
            setWinner(player);
            return;
          }
        } else counter = 0;
        initialY = initialY - 1;
      }
    }
  };

  const handleVerticalWinner = (player) => {
    for (let x = 0; x < slots[0].length; x++) {
      let counter = 0;
      for (let y = 0; y < slots.length - 1; y++) {
        if (slots[y][x] !== 0 && slots[y][x] === slots[y + 1][x]) {
          counter += 1;
          if (counter === 3) {
            setWinner(player);
          }
        } else {
          counter = 0;
        }
      }
    }
  };

  const handleHorizontalWinner = (player) => {
    for (let y = 0; y < slots.length; y++) {
      let counter = 0;
      for (let x = 0; x < slots[y].length - 1; x++) {
        if (slots[y][x] !== 0 && slots[y][x] === slots[y][x + 1]) {
          counter += 1;
          if (counter === 3) {
            setWinner(player);
          }
        } else {
          counter = 0;
        }
      }
    }
  };

  const handleReset = () => {
    window.location.reload();
  };

  return (
    <>
      <button onClick={handleReset} className={style.buttons}>
        Reset
      </button>
      {winner === 0 && (
        <p className={style.title}>
          Current player: {PLAYERS[currentPlayer - 1]}
        </p>
      )}
      {winner !== 0 && (
        <>
          <p className={style.title}>Winner: {WIN_PLAYERS[winner - 1]}</p>
        </>
      )}
      <div className={style.dash}>
        {slots.map((slotArray, indexX) => (
          <div className={style.row} key={indexX}>
            {slotArray.map((slot, indexY) => (
              <Slot
                key={`${indexX}_${indexY}`}
                slot={slot}
                x={indexX}
                y={indexY}
                handleClickSlot={handleClickSlot}
              />
            ))}
          </div>
        ))}
      </div>
    </>
  );
};

export default Board;
