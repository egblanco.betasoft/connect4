import { useState } from "react";
import "./App.css";
import Board from "./components/Board";

export const DEFAULT_BOARD = [
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0],
];

export const PLAYERS_COLORS = {
  red: "RED",
  yellow: "YELLOW",
};

function App() {
  const [slots, setSlots] = useState(DEFAULT_BOARD);

  return (
    <div className="App">
      <Board slots={slots} setSlots={setSlots} />
    </div>
  );
}

export default App;
